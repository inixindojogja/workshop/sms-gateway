<?php 

require './vendor/autoload.php';

use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;

// Configure client
$config = Configuration::getDefaultConfiguration();
$config->setApiKey('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTUzMzE4MzY0MywiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjI1MDcxLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.FtvZcHHe5nKqXNrpFMRsuK6Iz5h6J5B-STcvoLoeZok');
$apiClient = new ApiClient($config);
$messageClient = new MessageApi($apiClient);

// Get SMS Message Information
$messages = $messageClient->searchMessages(
    [
        'filters' => [
            [
                [
                    'field' => 'device_id',
                    'operator' => '=',
                    'value' => '97803'
                ],
                [
                    'field' => 'status',
                    'operator' => '=',
                    'value' => 'received'
                ],
                // [
                //     'field' => 'phone_number',
                //     'operator' => 'LIKE',
                //     'value' => '%62%'
                // ],
            ]
        ],
        'order_by' => [
            [
                'field' => 'created_at',
                'direction' => 'DESC'
            ],
        ],
        'limit'   => 5,
        'offset'  => 5
    ]
);


$dataPesan = [];

foreach ($messages['results'] as $message) {
    $dataPesan[] = [
        'status' => $message['status'],
        'message' => $message['message']
    ];
}


header('Content-Type: application/json');
echo json_encode([
    'success' => true,
    'message' => ($dataPesan)
]);