<?php

require 'vendor/autoload.php';

use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Model\SendMessageRequest;

// Configure client
$config = Configuration::getDefaultConfiguration();
$config->setApiKey('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTUzMzE4MzY0MywiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjI1MDcxLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.FtvZcHHe5nKqXNrpFMRsuK6Iz5h6J5B-STcvoLoeZok');
$apiClient = new ApiClient($config);
$messageClient = new MessageApi($apiClient);


$noHP = $_REQUEST['no_hp'];
$pesan = $_REQUEST['pesan'];

// Sending a SMS Message
$sendMessageRequest1 = new SendMessageRequest([
    'phoneNumber' => $noHP,
    'message' => $pesan,
    'deviceId' => 97803
]);
// $sendMessageRequest2 = new SendMessageRequest([
//     'phoneNumber' => '089685024091',
//     'message' => 'test2',
//     'deviceId' => 97803
// ]);
$sendMessages = $messageClient->sendMessages([
    $sendMessageRequest1,
    // $sendMessageRequest2
]);

header('Content-Type: application/json');
echo json_encode([
	'success' => true,
	'message' => 'Pesan Terkirim'
]);